import sidebar from './components/main/sidebar.vue';
import topnav from './components/main/topnav.vue';
import ytvideo from './components/widgets/ytvideo.vue';
import panel from './components/main/panel.vue';

export default {
    sidebar: sidebar,
    topnav: topnav,
    ytvideo: ytvideo,
    panel: panel,
};