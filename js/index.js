import Vue from 'vue';
import component_map from './component_map';

export { component_map as cm };
export class Simple {
    constructor(el){
        this.el = el;
        this.instance = null;

        this.vue_init_obj = {
            el: this.el,
            components: component_map,
            data: {},
            methods: {

            },
        }
    }

    setVueInitObj(obj_new) {
        this.vue_init_obj = obj_new;
    }

    getVueInitObj() {
        return this.vue_init_obj;
    }

    setNewMethods(methods) {
        Object.assign(this.vue_init_obj.methods, methods);
    }

    setNewData(data){
        Object.assign(this.vue_init_obj.data, data);
    }

    getInstance(){
        if (this.instance != null)
            return this.instance;

        this.instance = new Vue(this.vue_init_obj);

        return this.instance;
    }
}